package com.app.driverapp.utility;

import android.util.Log;

import java.util.HashMap;

import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;


public class PublishUtility 
{
	public static void publish(String channelName, HashMap<String,String> Message , PubNub pubnub)
	{
		if(channelName!=null){
			_publish(channelName, Message, pubnub);
		}
	}
	private static void _publish(final String channel, HashMap message, final PubNub pubnub)
	{
	try {
		Log.d("PK_PUBLISH",channel+"   "+message);
		pubnub.publish()
				.message(message)
				.channel(channel)
				.async(new PNCallback<PNPublishResult>() {
					@Override
					public void onResponse(PNPublishResult result, PNStatus status) {
						// handle publish result, status always present, result if successful
						// status.isError to see if error happened

						if(result!=null ){
							Log.d("PK_PUBLISH",result.getTimetoken().toString());
						}
					}
				});


	}
		catch (Exception e){}
	}

}
