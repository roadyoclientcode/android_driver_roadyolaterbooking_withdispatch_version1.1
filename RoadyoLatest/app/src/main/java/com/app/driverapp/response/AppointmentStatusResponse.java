package com.app.driverapp.response;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class AppointmentStatusResponse implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*""errNum":"49",
"errFlag":"1",
"errMsg":"Booking date or time not found.",
"tripsToday":"0",
"earningsToday":"0",
"wallet":"-137.48",
"balanceLimit":"5",
"lastTripTime":"2016-07-06 15:55:23",
"lastTripPrice":"9.5",
"carType":"Silver ",
"data":[
],"*/
	private int errNum;
	private int errFlag;
	private String errMsg;
	private String masStatus;
	private String tripsToday;
	private String earningsToday;
	private String wallet;
	private String balance;
	private String balanceLimit;
	private String lastTripPrice;
	private String lastTripTime;
	private String carType;


	public String getMasStatus() {
		return masStatus;
	}

	public void setMasStatus(String masStatus) {
		this.masStatus = masStatus;
	}
	
	@SerializedName ("data")
	private ArrayList<AppointmentDetailData>data;

	public ArrayList<AppointmentDetailData> getData() {
		return data;
	}
	public void setData(ArrayList<AppointmentDetailData> data) {
		this.data = data;
	}
	public int getErrNum() {
		return errNum;
	}
	public void setErrNum(int errNum) {
		this.errNum = errNum;
	}
	public int getErrFlag() {
		return errFlag;
	}
	public void setErrFlag(int errFlag) {
		this.errFlag = errFlag;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getTripsToday() {
		return tripsToday;
	}

	public void setTripsToday(String tripsToday) {
		this.tripsToday = tripsToday;
	}

	public String getEarningsToday() {
		return earningsToday;
	}

	public void setEarningsToday(String earningsToday) {
		this.earningsToday = earningsToday;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getBalanceLimit() {
		return balanceLimit;
	}

	public void setBalanceLimit(String balanceLimit) {
		this.balanceLimit = balanceLimit;
	}

	public String getLastTripPrice() {
		return lastTripPrice;
	}

	public void setLastTripPrice(String lastTripPrice) {
		this.lastTripPrice = lastTripPrice;
	}

	public String getLastTripTime() {
		return lastTripTime;
	}

	public void setLastTripTime(String lastTripTime) {
		this.lastTripTime = lastTripTime;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}
}
