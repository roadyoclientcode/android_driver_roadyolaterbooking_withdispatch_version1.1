package com.app.driverapp;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.pojo.MasterTripDetails;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by embed on 5/4/16.
 */
public class PaymentLocsFragment extends Fragment
{

    private View view;
    private TextView mDriverCashEarnings,
                     mDriverCardEarnings,
                     mTotalEarnings,
                     mCashCollected,
                     mPendingPayment,
                     mDriverIsDue,
                     mDriverHasToPay,
                     mAmountPaidLstPayRoll,
                     mPaymentDateLstPayRoll,
                     mPendingPaymentLstPayRoll,
                     mTotalSetteled;
    private SessionManager sessionManager;
    private ProgressDialog mdialog;
    private MasterTripDetails mMasterTripDetails;
    private TextView mDriverCashEarningsHeader;
    private TextView tvPendingPaymentTitle;
    private TextView tvDCardEarningsHeader;
    private TextView tvTotalEarningsHeader;
    private TextView tvCashCollectedHeader;
    private TextView tvTotalSetteledHeader;
    private TextView tvPendingPaymentHeader;
    private TextView tvLastPayment;
    private TextView tvAmountPaidHeader;
    private TextView tvPaymentDateHeader;
    private Typeface font,fontBold;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        sessionManager=new SessionManager(getActivity());
        view = inflater.inflate(R.layout.payment_locs_layout, container, false);
        mInitLayoutId(view);
        getMasterTripDetails();
        return view;
    }


    private void mInitLayoutId(View view)
    {
        font = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Regular.ttf");
        fontBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Bold.ttf");

        mDriverCashEarnings= (TextView) view.findViewById(R.id.tvDCashEarningsValue);
        mDriverCashEarningsHeader= (TextView) view.findViewById(R.id.tvDCashEarningsHeader);
        mDriverCardEarnings= (TextView) view.findViewById(R.id.tvDCardEarningsValue);
        mTotalEarnings= (TextView) view.findViewById(R.id.tvTotalEarningsValue);
        mCashCollected= (TextView) view.findViewById(R.id.tvCashCollectedValue);
        mPendingPayment= (TextView) view.findViewById(R.id.tvPendingPaymentValue);
        mDriverIsDue= (TextView) view.findViewById(R.id.tvDriverIsDueValue);
        mDriverHasToPay= (TextView) view.findViewById(R.id.tvDriverHasToPayValue);
        mAmountPaidLstPayRoll= (TextView) view.findViewById(R.id.tvAmountPaidValue);
        mPaymentDateLstPayRoll= (TextView) view.findViewById(R.id.tvPaymentDateValue);
        mPendingPaymentLstPayRoll= (TextView) view.findViewById(R.id.tvPendingPaymentLastPayrollValue);
        mTotalSetteled= (TextView) view.findViewById(R.id.tvTotalsetteledValue);
        tvPendingPaymentTitle= (TextView) view.findViewById(R.id.tvPendingPaymentTitle);
        tvDCardEarningsHeader= (TextView) view.findViewById(R.id.tvDCardEarningsHeader);
        tvTotalEarningsHeader= (TextView) view.findViewById(R.id.tvTotalEarningsHeader);
        tvCashCollectedHeader= (TextView) view.findViewById(R.id.tvCashCollectedHeader);
        tvTotalSetteledHeader= (TextView) view.findViewById(R.id.tvTotalSetteledHeader);
        tvPendingPaymentHeader= (TextView) view.findViewById(R.id.tvPendingPaymentHeader);
        tvLastPayment= (TextView) view.findViewById(R.id.tvLastPayment);
        tvAmountPaidHeader= (TextView) view.findViewById(R.id.tvAmountPaidHeader);
        tvPaymentDateHeader= (TextView) view.findViewById(R.id.tvPaymentDateHeader);

        mDriverCashEarnings.setTypeface(font);
        mDriverCashEarningsHeader.setTypeface(font);
        mDriverCardEarnings.setTypeface(font);
        mTotalEarnings.setTypeface(font);
        mCashCollected.setTypeface(font);
        mPendingPayment.setTypeface(font);
        mDriverIsDue.setTypeface(font);
        mDriverHasToPay.setTypeface(font);
        mAmountPaidLstPayRoll.setTypeface(font);
        mPaymentDateLstPayRoll.setTypeface(font);
        mPendingPaymentLstPayRoll.setTypeface(font);
        mTotalSetteled.setTypeface(font);
        tvPendingPaymentTitle.setTypeface(fontBold);
        tvDCardEarningsHeader.setTypeface(font);
        tvTotalEarningsHeader.setTypeface(font);
        tvCashCollectedHeader.setTypeface(font);
        tvTotalSetteledHeader.setTypeface(font);
        tvPendingPaymentHeader.setTypeface(font);
        tvLastPayment.setTypeface(fontBold);
        tvAmountPaidHeader.setTypeface(font);
        tvPaymentDateHeader.setTypeface(font);
    }

    public void setValues(MasterTripDetails mMasterTripDetails)
    {
        String Value;
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getDriverCashEarning()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getDriverCashEarning()));
            mDriverCashEarnings.setText( VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getDriverCardEarning()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getDriverCardEarning()));
            mDriverCardEarnings.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getDriverTotalEarning()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getDriverTotalEarning()));
            mTotalEarnings.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getDriverCashColleted()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getDriverCashColleted()));
            mCashCollected.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);

        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getPending()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getPending()));
            mPendingPayment.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);

        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getDriverToPay()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getDriverToPay()));
            mDriverIsDue.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getTotalDue()))
        {
           // Value = String.format("%.2f", mMasterTripDetails.getPaymentLoc().getTotalDue());
            mDriverHasToPay.setText(VariableConstants.CURRENCY_SYMBOL+" "+mMasterTripDetails.getPaymentLoc().getTotalDue());
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getLastPayAmount()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getLastPayAmount()));
            mAmountPaidLstPayRoll.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getLastPayDate()))
        {
            Value = mMasterTripDetails.getPaymentLoc().getLastPayDate();
            mPaymentDateLstPayRoll.setText(Value);

        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getPending()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getPending()));
            mPendingPaymentLstPayRoll.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);

        }
        if(!"".equals(mMasterTripDetails.getPaymentLoc().getTotalReceived()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getPaymentLoc().getTotalReceived()));
            mTotalSetteled.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);

        }
    }

    private void getMasterTripDetails()
    {
        if (Utility.isNetworkAvailable(getActivity()))
        {
            String sessionToken=sessionManager.getSessionToken();
            String deviceid=Utility.getDeviceId(getActivity());
            String date=new Utility().getCurrentGmtTime();
            final String mparams[]={sessionToken,deviceid,date };
            mdialog = Utility.GetProcessDialog(getActivity());
            mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
            mdialog.show();
            mdialog.setCancelable(false);
            RequestQueue queue = Volley.newRequestQueue(getActivity());  // this = context
            StringRequest postRequest = new StringRequest(Request.Method.POST, VariableConstants.hostUrl+"getMasterTripDetails",responseListenerofRejectNotification,errorListener )
            {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("ent_sess_token",mparams[0]);
                    params.put("ent_dev_id",mparams[1]);
                    params.put("ent_date_time", mparams[2]);
                    return params;
                }
            };
            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            queue.add(postRequest);
        }
    }


    Response.Listener<String> responseListenerofRejectNotification=new Response.Listener<String>()
    {

        @Override
        public void onResponse(String response)
        {

            Gson gson = new Gson();
            mMasterTripDetails=gson.fromJson(response, MasterTripDetails.class);

            Log.i("masrter", "payment= " + response);
            try
            {
                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }
              setValues(mMasterTripDetails);
            }
            catch (Exception e)
            {
                Log.i("", "Exception= " + e.getMessage());
            }

        }
    };

    Response.ErrorListener errorListener=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            try
            {
                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }

            }
            catch (Exception e)
            {

            }
        }
    };

}
