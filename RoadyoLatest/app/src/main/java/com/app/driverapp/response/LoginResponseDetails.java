package com.app.driverapp.response;

public class LoginResponseDetails
{
	/*{
		"errNum":"9",
			"errFlag":"0",
			"errMsg":"Login completed!",
			"data":{
		"token":"33j7cR7MtmAceslzp1FbHA3531393731303730333131353537j7cR7MtmAceslzp1FbHA",
				"expiryLocal":"2586-11-22 07:15:20",
				"expiryGMT":"2586-11-22 07:15:20",
				"fname":"Robin",
				"presenseChn":"presenceChn_roadyo1.0_channel",
				"lname":"Hood",
				"profilePic":"",
				"medicalLicenseNum":"",
				"flag":"1",
				"joined":"2016-06-27 16:21:24",
				"email":"robin@gmail.com",
				"susbChn":"qd_351971070311557",
				"chn":"roadyo1.0_channel",
				"listner":"qdl_351971070311557",
				"status":"4",
				"driverid":"292",
				"vehTypeId":"29",
				"cityid":"2",
				"carType":"Silver ",
				"typeImage":"2265326514108.png",
				"pub":"pub-c-aa67f186-575e-4428-a73b-a7c2e35ccde7",
				"sub":"sub-c-9abf75a0-1ea2-11e6-8b91-02ee2ddab7fe",
				"phone":"123"
	}
	}*/
	private String token;
	private String expiryLocal;
	private String expiryGMT;
	private String profilePic;
	private String flag;
	private String joined;
	private String chn;
	private String apiKey;
	private String email;
	private String fname;
	private String lname;
	private String susbChn;
	private String listner;
	private String vehTypeId;
	private String typeImage;
	private String cityid;
	private String carType;
	private String driverid;
	private String phone;
	private String presenseChn;


	public String getPresenseChn() {
		return presenseChn;
	}

	public void setPresenseChn(String presenseChn) {
		this.presenseChn = presenseChn;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getDriverid() {
		return driverid;
	}

	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getVehTypeId() {
		return vehTypeId;
	}
	public void setVehTypeId(String vehTypeId) {
		this.vehTypeId = vehTypeId;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSusbChn() {
		return susbChn;
	}
	public void setSusbChn(String susbChn) {
		this.susbChn = susbChn;
	}
	public String getListner() {
		return listner;
	}
	public void setListner(String listner) {
		this.listner = listner;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getExpiryLocal() {
		return expiryLocal;
	}
	public void setExpiryLocal(String expiryLocal) {
		this.expiryLocal = expiryLocal;
	}
	public String getExpiryGMT() {
		return expiryGMT;
	}
	public void setExpiryGMT(String expiryGMT) {
		this.expiryGMT = expiryGMT;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getJoined() {
		return joined;
	}
	public void setJoined(String joined) {
		this.joined = joined;
	}
	public String getChn() {
		return chn;
	}
	public void setChn(String chn) {
		this.chn = chn;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTypeImage() {
		return typeImage;
	}

	public void setTypeImage(String typeImage) {
		this.typeImage = typeImage;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}
}
