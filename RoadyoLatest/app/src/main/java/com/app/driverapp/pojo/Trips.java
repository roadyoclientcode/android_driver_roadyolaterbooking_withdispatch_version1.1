package com.app.driverapp.pojo;

import java.io.Serializable;

/**
 * Created by embed on 7/4/16.
 */
public class Trips implements Serializable
{

    private String canceled_toadys;

    private String week_earnings;

    private String canceled_week;

    private String toadys_earning;

    private String cmpltApts_month;

    private String canceld_month;

    private String month_earnings;

    private String cmpltApts_week;

    private String cmpltApts_toady;

    public String getCanceled_toadys ()
    {
        return canceled_toadys;
    }

    public void setCanceled_toadys (String canceled_toadys)
    {
        this.canceled_toadys = canceled_toadys;
    }

    public String getWeek_earnings ()
    {
        return week_earnings;
    }

    public void setWeek_earnings (String week_earnings)
    {
        this.week_earnings = week_earnings;
    }

    public String getCanceled_week ()
    {
        return canceled_week;
    }

    public void setCanceled_week (String canceled_week)
    {
        this.canceled_week = canceled_week;
    }

    public String getToadys_earning ()
    {
        return toadys_earning;
    }

    public void setToadys_earning (String toadys_earning)
    {
        this.toadys_earning = toadys_earning;
    }

    public String getCmpltApts_month ()
    {
        return cmpltApts_month;
    }

    public void setCmpltApts_month (String cmpltApts_month)
    {
        this.cmpltApts_month = cmpltApts_month;
    }

    public String getCanceld_month ()
    {
        return canceld_month;
    }

    public void setCanceld_month (String canceld_month)
    {
        this.canceld_month = canceld_month;
    }

    public String getMonth_earnings ()
    {
        return month_earnings;
    }

    public void setMonth_earnings (String month_earnings)
    {
        this.month_earnings = month_earnings;
    }

    public String getCmpltApts_week ()
    {
        return cmpltApts_week;
    }

    public void setCmpltApts_week (String cmpltApts_week)
    {
        this.cmpltApts_week = cmpltApts_week;
    }

    public String getCmpltApts_toady ()
    {
        return cmpltApts_toady;
    }

    public void setCmpltApts_toady (String cmpltApts_toady)
    {
        this.cmpltApts_toady = cmpltApts_toady;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [canceled_toadys = "+canceled_toadys+", week_earnings = "+week_earnings+", canceled_week = "+canceled_week+", toadys_earning = "+toadys_earning+", cmpltApts_month = "+cmpltApts_month+", canceld_month = "+canceld_month+", month_earnings = "+month_earnings+", cmpltApts_week = "+cmpltApts_week+", cmpltApts_toady = "+cmpltApts_toady+"]";
    }
}
