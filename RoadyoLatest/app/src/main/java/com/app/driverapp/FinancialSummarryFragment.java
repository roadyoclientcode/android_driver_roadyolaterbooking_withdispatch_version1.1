package com.app.driverapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

public class FinancialSummarryFragment extends Fragment {


    ViewPager mViewPager;
    CustomPagerAdapter mCustomPagerAdapter;
    private View view;
    PagerSlidingTabStrip tabsStrip;




    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.financial_summarry_frag, container, false);
        mCustomPagerAdapter=new CustomPagerAdapter(getFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setAdapter(mCustomPagerAdapter);
        tabsStrip = (PagerSlidingTabStrip)view.findViewById(R.id.tabs);
        tabsStrip.setViewPager(mViewPager);



        final LinearLayout pagetabber = (LinearLayout)tabsStrip.getChildAt(0);

        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int lightgray = Color.parseColor("#A0A0A0");
                int darkgraycolor = Color.parseColor("#333333");
                if (position == 0) {

                    // int lightgray = Color.parseColor("");
                    TextView textView1 = (TextView) pagetabber.getChildAt(0);
                    textView1.setTextColor(darkgraycolor);
                    TextView textView2 = (TextView) pagetabber.getChildAt(1);
                    textView2.setTextColor(lightgray);

                } else if (position == 1) {
                    // int darkgraycolor = Color.parseColor("#A0A0A0");//c5c4c4  bcbcbc
                    TextView textView1 = (TextView) pagetabber.getChildAt(1);
                    textView1.setTextColor(darkgraycolor);
                    TextView textView2 = (TextView) pagetabber.getChildAt(0);
                    textView2.setTextColor(lightgray);

                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }
}
