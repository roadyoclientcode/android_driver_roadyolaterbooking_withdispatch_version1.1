package com.app.driverapp;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.pojo.MasterTripDetails;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by embed on 5/4/16.
 */
public class TripsFragment extends Fragment
{
    private View view;
    TextView mTodaysCompletedTrips,
             mTodaysCanceledTrips,
             mTodaysEarnings,
             mWeeklyCompletedTrips,
             mWeeklyCanceledTrips,
             mWeeklyEarnings,
             mMonthlyCompletedTrips,
             mMonthlyCanceledTrps,
             mMonthlyEarnings,
            tvTodaysTrips,
            tvCompletedTripsHeader,
            tvCanceledTripsHeader,
            tvTodaysEarningsHeader,
            tvWeeklyTrips,
            tvCompletedTripsWeeklyHeader,
            tvCanceledTripsWeeklyHeader,
            tvWeeklyEarningsHeader,
            tvMonthlyTrips,
            tvCompletedTripsMonthlyHeader,
            tvCanceledTripsMonthlyyHeader,
            tvMonthlyEarningsHeader
                     ;
    private SessionManager sessionManager;
    private ProgressDialog mdialog;
    private MasterTripDetails mMasterTripDetails;
    private Typeface font,fontBold;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        sessionManager=new SessionManager(getActivity());
        view = inflater.inflate(R.layout.trips_layout, container, false);
        mInitLayoutId(view);
        getMasterTripDetails();
        return view;
    }

    private void mInitLayoutId(View view)
    {
        font = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Regular.ttf");
        fontBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Bold.ttf");

        mTodaysCompletedTrips= (TextView) view.findViewById(R.id.tvCompletedTripsValue);
        mTodaysCanceledTrips= (TextView) view.findViewById(R.id.tvCanceledTripsValue);
        mTodaysEarnings= (TextView) view.findViewById(R.id.tvtodaysEarningsValue);
        mWeeklyCompletedTrips= (TextView) view.findViewById(R.id.tvCompletedTripsWeeklyValue);
        mWeeklyCanceledTrips= (TextView) view.findViewById(R.id.tvCanceledTripsWeeklyValue);
        mWeeklyEarnings= (TextView) view.findViewById(R.id.tvWeklyEarningsValue);
        mMonthlyCompletedTrips= (TextView) view.findViewById(R.id.tvCompletedTripsMonthlyValue);
        mMonthlyCanceledTrps= (TextView) view.findViewById(R.id.tvCanceledTripsMonthlyValue);
        mMonthlyEarnings= (TextView) view.findViewById(R.id.tvMonthlyEarningsValue);
        tvTodaysTrips= (TextView) view.findViewById(R.id.tvTodaysTrips);
        tvCompletedTripsHeader= (TextView) view.findViewById(R.id.tvCompletedTripsHeader);
        tvCanceledTripsHeader= (TextView) view.findViewById(R.id.tvCanceledTripsHeader);
        tvTodaysEarningsHeader= (TextView) view.findViewById(R.id.tvTodaysEarningsHeader);
        tvWeeklyTrips= (TextView) view.findViewById(R.id.tvWeeklyTrips);
        tvCompletedTripsWeeklyHeader= (TextView) view.findViewById(R.id.tvCompletedTripsWeeklyHeader);
        tvCanceledTripsWeeklyHeader= (TextView) view.findViewById(R.id.tvCanceledTripsWeeklyHeader);
        tvWeeklyEarningsHeader= (TextView) view.findViewById(R.id.tvWeeklyEarningsHeader);
        tvMonthlyTrips= (TextView) view.findViewById(R.id.tvMonthlyTrips);
        tvCompletedTripsMonthlyHeader= (TextView) view.findViewById(R.id.tvCompletedTripsMonthlyHeader);
        tvCanceledTripsMonthlyyHeader= (TextView) view.findViewById(R.id.tvCanceledTripsMonthlyyHeader);
        tvMonthlyEarningsHeader= (TextView) view.findViewById(R.id.tvMonthlyEarningsHeader);

        mTodaysCompletedTrips.setTypeface(font);
        mTodaysCanceledTrips.setTypeface(font);
        mTodaysEarnings.setTypeface(font);
        mWeeklyCompletedTrips.setTypeface(font);
        mWeeklyCanceledTrips.setTypeface(font);
        mWeeklyEarnings.setTypeface(font);
        mMonthlyCompletedTrips.setTypeface(font);
        mMonthlyCanceledTrps.setTypeface(font);
        mMonthlyEarnings.setTypeface(font);
        tvCompletedTripsHeader.setTypeface(font);
        tvCanceledTripsHeader.setTypeface(font);
        tvTodaysEarningsHeader.setTypeface(font);
        tvWeeklyTrips.setTypeface(font);
        tvCompletedTripsWeeklyHeader.setTypeface(font);
        tvCanceledTripsWeeklyHeader.setTypeface(font);
        tvWeeklyEarningsHeader.setTypeface(font);
        tvMonthlyTrips.setTypeface(font);
        tvCompletedTripsMonthlyHeader.setTypeface(font);
        tvCanceledTripsMonthlyyHeader.setTypeface(font);
        tvMonthlyEarningsHeader.setTypeface(font);
        tvTodaysTrips.setTypeface(font);
    }

    public void setValues(MasterTripDetails mMasterTripDetails)
    {
        String Value;
        if(!"".equals(mMasterTripDetails.getTrips().getCmpltApts_toady()))
        {
            Value = mMasterTripDetails.getTrips().getCmpltApts_toady();
            mTodaysCompletedTrips.setText(Value);
        }
        if(!"".equals(mMasterTripDetails.getTrips().getCanceled_toadys()))
        {
            Value = mMasterTripDetails.getTrips().getCanceled_toadys();
            mTodaysCanceledTrips.setText(Value);
        }
        if(!"".equals(mMasterTripDetails.getTrips().getToadys_earning()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getTrips().getToadys_earning()));
            mTodaysEarnings.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }

        if(!"".equals(mMasterTripDetails.getTrips().getCmpltApts_week()))
        {
            Value = mMasterTripDetails.getTrips().getCmpltApts_week();
            mWeeklyCompletedTrips.setText(Value);
        }
        if(!"".equals(mMasterTripDetails.getTrips().getCanceled_week()))
        {
            Value = mMasterTripDetails.getTrips().getCanceled_week();
            mWeeklyCanceledTrips.setText(Value);
        }
        if(!"".equals(mMasterTripDetails.getTrips().getWeek_earnings()))
        {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getTrips().getWeek_earnings()));
            mWeeklyEarnings.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);

        }
        if(!"".equals(mMasterTripDetails.getTrips().getCmpltApts_month()))
        {
            Value =mMasterTripDetails.getTrips().getCmpltApts_month();
            mMonthlyCompletedTrips.setText(Value);

        }
        if(!"".equals(mMasterTripDetails.getTrips().getCanceld_month()))
        {
            Value =mMasterTripDetails.getTrips().getCanceld_month();
            mMonthlyCanceledTrps.setText(Value);

        }
        if(!"".equals(mMasterTripDetails.getTrips().getMonth_earnings())) {
            Value = String.format("%.2f", Double.parseDouble(mMasterTripDetails.getTrips().getMonth_earnings()));
            mMonthlyEarnings.setText(VariableConstants.CURRENCY_SYMBOL+" "+Value);
        }

    }

    private void getMasterTripDetails()
    {



        if (Utility.isNetworkAvailable(getActivity()))
        {
            String sessionToken=sessionManager.getSessionToken();
            String deviceid=Utility.getDeviceId(getActivity());
            String date=new Utility().getCurrentGmtTime();
            final String mparams[]={sessionToken,deviceid,date };
            mdialog = Utility.GetProcessDialog(getActivity());
            mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
            mdialog.show();
            mdialog.setCancelable(false);
            RequestQueue queue = Volley.newRequestQueue(getActivity());  // this = context
            StringRequest postRequest = new StringRequest(Request.Method.POST, VariableConstants.hostUrl+"getMasterTripDetails",responseListenerofRejectNotification,errorListener )
            {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("ent_sess_token",mparams[0]);
                    params.put("ent_dev_id",mparams[1]);
                    params.put("ent_date_time", mparams[2]);
                    return params;
                }
            };
            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            queue.add(postRequest);
        }
    }


    Response.Listener<String> responseListenerofRejectNotification=new Response.Listener<String>()
    {

        @Override
        public void onResponse(String response)
        {

            Gson gson = new Gson();
            mMasterTripDetails=gson.fromJson(response, MasterTripDetails.class);

            Log.i("", "MasterTripDetails= " + response);
            try
            {
                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }
                setValues(mMasterTripDetails);
            }
            catch (Exception e)
            {
                Log.i("", "Exception= " + e.getMessage());
            }

        }
    };

    Response.ErrorListener errorListener=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            try
            {
                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }

            }
            catch (Exception e)
            {

            }
        }
    };



}
