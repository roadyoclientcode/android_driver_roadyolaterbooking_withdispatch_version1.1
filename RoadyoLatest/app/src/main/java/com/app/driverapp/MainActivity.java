package com.app.driverapp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.apache.http.NameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.driverapp.calander.MyBookingsFrag;
import com.app.driverapp.response.LogoutResponse;
import com.app.driverapp.utility.MyNetworkChangeListner;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.gson.Gson;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

@SuppressLint("Wakelock")
public class MainActivity extends FragmentActivity
{
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private BackGroundTaskForLogout backGroundTaskForLogout;
	private ActionBarDrawerToggle mDrawerToggle;
	private String driverChannelName;
	private Button /*online_button,offline_button*/schedule_button;
	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private int currentTabStatus = -1;
	private android.app.ActionBar actionBar;
	private ImageButton button_menu;
	private TextView nav_img;
	//private ImageButton button_right_menu;
	private TextView activity_main_content_title;
	public static boolean isResponse = true;
	private static PubNub pubnub,pubnub2;
	private SessionManager session;
	private PowerManager.WakeLock wl;
	//public static TextView schedule;

	
	//private String message,action;
	Context context = MainActivity.this;
	static Activity mainActivity;
	private boolean doubleBackToExitPressedOnce=false;
	private NetworkChangeReceiver networkChangeReceiver;

	@SuppressLint({ "NewApi", "Wakelock" })
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(savedInstanceState!=null){
			currentTabStatus=savedInstanceState.getInt("CURRENT_TAB");
		}
		mainActivity=this;

		pubnub=ApplicationController.getInstacePubnub();
		pubnub2=ApplicationController.getInstacePubnub();

		networkChangeReceiver=new NetworkChangeReceiver();
		networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
			@Override
			public void onNetworkStateChanges(boolean nwStatus) {
				Utility.printLog("PK Network Status MainActvty "+nwStatus);
				if(nwStatus)
				{
					pubnub2.reconnect();
				}

			}
		});
		/*new NetworkChangeReceiver.NetworkNt2(new MyNetworkChangeListner() {
			@Override
			public void onNetworkStateChanges(boolean nwStatus) {

			}
		});*/
		session = new SessionManager(this);
		if(session.getIsOnTheJob())
		{
			subscribe(session.getPresenceChannel());
		}


		driverChannelName = session.getSubscribeChannel();
		session.setIsHomeFragmentisOpened(true);

		startMyservice();

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_main_drawer);
		overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);

		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu12);

		navDrawerItems = new ArrayList<NavDrawerItem>();
		// adding nav drawer items to array 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		// Pages
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		// What's hot, We  will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		//setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),	navDrawerItems);
		mDrawerList.setAdapter(adapter);
		// enabling action bar app icon and behaving it as toggle button
		actionBar = getActionBar();
		actionBar.setTitle(getResources().getString(R.string.driverapptext));
		actionBar.setIcon(android.R.color.transparent);
		initActionbar() ;
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.drawable.selector_for_menu_button, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
				)
		{
			public void onDrawerClosed(View view) 
			{
				getActionBar().setTitle("");
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) 
			{
				getActionBar().setTitle("");
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		if (savedInstanceState == null)
		{
			// on first time display view for first nav item
			if (session.getFlagCalender()) 
			{
				session.setFlagCalender(false);
				displayView(1);
			}
			else 
			{
				displayView(0);
			}
		}
	}

	public PubNub getPubnubObject()
	{
		return pubnub;
	}

	/**
	 * Initializes the ActionBar for the <code>Activity</code>
	 */
	@SuppressLint("NewApi")
	private void initActionbar() 
	{
		//BitmapDrawable bg = (BitmapDrawable) getResources().getDrawable(R.drawable.login_screen_navigation_bar);
		//actionBar.setBackgroundDrawable(bg);

		Typeface fontBold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");

		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#333333")));
		LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout mActionBarCustom = (LinearLayout)inflater.inflate(R.layout.customactionbar, null);
		button_menu=(ImageButton)mActionBarCustom.findViewById(R.id.button_menu);
		button_menu.setVisibility(View.GONE);
		//schedule = (TextView)findViewById(R.id.schedule);
		//online_button = (Button)mActionBarCustom.findViewById(R.id.online_button);
		//offline_button = (Button)mActionBarCustom.findViewById(R.id.offline_button);
		actionBar.setCustomView(mActionBarCustom);
		actionBar.setDisplayShowCustomEnabled(true);
		
		//online_button.setOnClickListener(this);
		//offline_button.setOnClickListener(this);
		activity_main_content_title=(TextView)mActionBarCustom.findViewById(R.id.activity_main_content_title);
		activity_main_content_title.setTypeface(fontBold);
		/*double scaler[]=Scaler.getScalingFactor(this);
		int padding = (int)Math.round(120*scaler[0]);
		activity_main_content_title.setPadding(padding, 0, 0, 0);*/
		nav_img = (TextView)mActionBarCustom.findViewById(R.id.nav_img);
		/*schedule_button = (Button)mActionBarCustom.findViewById(R.id.schedule);
		
		schedule_button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(MainActivity.this,PendingBooking.class);
				startActivity(intent);
				finish();
			}
		});*/
	}

	public TextView getTitleTextview()
	{
		return activity_main_content_title;
	}

	public ImageButton getLeftmenubutton()
	{
		return button_menu;
	}
	public android.app.ActionBar getmActionBar()
	{
		return actionBar;
	}

	/**
	 * Slide menu item click listener
	 */
	private class SlideMenuClickListener implements
	ListView.OnItemClickListener 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id)
		{
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) 
		{
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	/**
	 * Displaying fragment view for selected nav drawer list item
	 * */
	@SuppressWarnings("deprecation")
	private void displayView(int position)
	{
		// update the main content by replacing fragments
		if (currentTabStatus==position) 
		{
			mDrawerLayout.closeDrawer(mDrawerList);
		} 
		else 
		{
			Fragment fragment = null;
			switch (position)
			{
			case 0:
				currentTabStatus=0;
				fragment = new HomeFragment2();
				activity_main_content_title.setVisibility(View.GONE);
				nav_img.setVisibility(View.VISIBLE);
			//	nav_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.roadyo_logo));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 1:
				currentTabStatus=1;
				fragment = new MyBookingsFrag();
				nav_img.setVisibility(View.GONE);
				activity_main_content_title.setVisibility(View.VISIBLE);
				activity_main_content_title.setText(getResources().getString(R.string.appointment));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 2:
				currentTabStatus=2;
				fragment = new Profile_Fragment();
				nav_img.setVisibility(View.GONE);
				activity_main_content_title.setVisibility(View.VISIBLE);
				activity_main_content_title.setText(getResources().getString(R.string.myprofile));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 3:
				//fragment = new ProfileFragment();
				currentTabStatus=3;
				fragment = new FinancialSummarryFragment();
				nav_img.setVisibility(View.GONE);
				activity_main_content_title.setVisibility(View.VISIBLE);
				activity_main_content_title.setText(getResources().getString(R.string.FINANCIAL_SUMMARRY));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 4:
				//fragment = new PagesFragment();
				currentTabStatus=4;
				fragment = new InviteFragmentNew();
				nav_img.setVisibility(View.GONE);
				activity_main_content_title.setVisibility(View.VISIBLE);
				activity_main_content_title.setText(getResources().getString(R.string.invite));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 5:
				//fragment = new WhatsHotFragment();
				currentTabStatus=5;
				fragment = new AppInfo();
				nav_img.setVisibility(View.GONE);
				activity_main_content_title.setVisibility(View.VISIBLE);
				activity_main_content_title.setText(getResources().getString(R.string.appinfo));
				//schedule_button.setVisibility(View.VISIBLE);
				break;
			case 6:
				currentTabStatus=6;
				ErrorMessageForLogout(getResources().getString(R.string.logout),getResources().getString(R.string.logoutmessage));
				break;
			default:
				break;
			}

			if (fragment != null)
			{
				performTransaction(fragment) ;
				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);
				mDrawerLayout.closeDrawer(mDrawerList);
			} 
			else 
			{
				// error in creating fragment
				Utility.printLog("MainActivity", "Error in creating fragment");
			}
		}
	}

	public void performTransaction(Fragment frag) 
	{
		try {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace(R.id.frame_container, mFragmentStack.push(frag));
			ft.commit();
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) 
	{
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public static String getDeviceId(Context context)
	{
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		if(currentTabStatus==-1){
			currentTabStatus=0;
		}
		displayView(currentTabStatus);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		wl.acquire();
		/*ConnectionDetector connectionDetector=new ConnectionDetector(MainActivity.this);
		if (connectionDetector.isConnectingToInternet()) 
		{
			getAppointmentStatus();
		}*/

//		session.setIsFlagForOther(false);
		session.setIsHomeFragmentisOpened(true);
		isResponse = true;
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();


	wl.release();	
	}


	private Stack<Fragment> mFragmentStack = new Stack<Fragment>();

	@Override
	public void onBackPressed()
	{

		try {
			mExitFromApp();
			/*mFragmentStack.pop();
			if (mFragmentStack.size() > 0 && (currentTabStatus != 0)) 
			{
				*//*FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				ft.replace(R.id.frame_container, mFragmentStack.peek());
				ft.commit();*//*
				displayView(0);
			}
			else if (currentTabStatus == 0) 
			{
				mExitFromApp();
			}
			else
			{
				finish();
			}*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}

	private void mExitFromApp() {

		if (doubleBackToExitPressedOnce) {
			finish();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce=false;
			}
		}, 2000);
	}

	private void startLogout()
	{
		SessionManager sessionManager=new SessionManager(this);
		unSubscribe();
		sessionManager.logoutUser();
		NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		stopMyservice();

		Intent intent=new Intent(MainActivity.this, SplashActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	private void ErrorMessageForLogout(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNegativeButton(getResources().getString(R.string.logout),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

				dialog.dismiss();
				logoutUser();
			}
		});

		builder.setPositiveButton(getResources().getString(R.string.cancle),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				currentTabStatus = -1;
				dialog.dismiss();
			}
		});
		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	private void logoutUser()
	{
		SessionManager sessionManager=new SessionManager(this);
		//Utility utility=new Utility();
		String deviceid=Utility.getDeviceId(this);
		Utility utility=new Utility();
		String sessionToken=sessionManager.getSessionToken();
		String logoutType=VariableConstants.USERTYPE;
		String currenttime=utility.getCurrentGmtTime();
		String params[]={sessionToken,deviceid,logoutType,currenttime};
		backGroundTaskForLogout=new BackGroundTaskForLogout();
		backGroundTaskForLogout.execute(params);
	}

	private class BackGroundTaskForLogout extends android.os.AsyncTask<String, Void, LogoutResponse>
	{
		private Utility utility=new Utility();
		private List<NameValuePair>logoutParamList;
		private String logoutResponse;
		private LogoutResponse logoutUser;
		private boolean isSuccess=true;
		private  ProgressDialog mdialog;

		@Override
		protected LogoutResponse doInBackground(String... params) 
		{
			try {
				logoutParamList = utility.getLogoutParameter(params);
				logoutResponse =  utility.makeHttpRequest(VariableConstants.logOut_url,VariableConstants.methodeName,logoutParamList);
				Utility.printLog("logoutResponse"+logoutResponse);
				if (logoutResponse!=null)
				{
					Gson gson = new Gson();
					logoutUser=gson.fromJson(logoutResponse, LogoutResponse.class);
				}
				else 
				{
					isSuccess=false;
				}
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
			return logoutUser;
		}


		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			mdialog=Utility.GetProcessDialog(MainActivity.this);
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
		}
		@Override
		protected void onPostExecute(LogoutResponse result)
		{
			super.onPostExecute(result);
			if (mdialog!=null)
			{
				mdialog.dismiss();
				mdialog.cancel();
				mdialog=null;
			}
			//	Error Codes:
			//ErrorNumber -> (ErrorFlag) ErrorMessage
			if (isSuccess)
			{
				try
				{
					if (result.getErrFlag()==0 && result.getErrNum()==29)
					{
						///29 -> (0) Logged out!
						NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.cancelAll();
						startLogout();
					}
					else if (result.getErrFlag()==1&&result.getErrNum()==1)
					{
						//1 -> (1) Mandatory field missing
						ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);

					}
					else if (result.getErrFlag()==1&&result.getErrNum()==6)
					{
						//6 -> (1) Session token expired, please login.
						ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

					}
					else if (result.getErrFlag()==1&&result.getErrNum()==7)
					{
						//7 -> (1) Invalid token, please login or register.
						ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

					}
					else if (result.getErrFlag()==1&&result.getErrNum()==3)
					{
						//3 -> (1) Error occurred while processing your request.
						ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);
					}
				}catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else 
			{
				// some server issue ocured
				ErrorMessage(getResources().getString(R.string.error),"Request timeout",false);
			}
		}

		private void ErrorMessageForSessionExpoired(String title,final String message,final int errorFlag,final int errornum)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{     

					if (errornum==6) 
					{
						//6 -> (1) Session token expired, please login.
						startLogout();
					}
					else if (errornum==7) 
					{
						//7 -> (1) Invalid token, please login or register.
						startLogout();
					}
					dialog.dismiss();
				}
			});

			builder.setNegativeButton(getResources().getString(R.string.cancelbutton),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

		private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (flageforSwithchActivity) 
					{
						MainActivity.this.finish();
					}
					else
					{
						// only show message  
					}
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}
	}








	public void stopMyservice()
	{
//		stopService(ApplicationController.getMyServiceInstance());
		stopService(new Intent(MainActivity.this,MyService.class));
	}
	public void startMyservice()
	{
		//startService(ApplicationController.getMyServiceInstance());
		startService(new Intent(MainActivity.this,MyService.class));
	}


	public void subscribe(final String channel)
	{
		try{
			pubnub2.subscribe()
					.withPresence()
					.channels(Arrays.asList(channel)).execute(); // subscribe to channels.execute();
		}catch (Exception e){
			e.printStackTrace();
		}



		pubnub2.addListener(new SubscribeCallback() {
			@Override
			public void status(PubNub pubnub, PNStatus status) {
				// handle any status
				if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
					// internet got lost, do some magic and call reconnect when ready
					//pubnub.reconnect();
				} else if (status.getCategory() == PNStatusCategory.PNTimeoutCategory) {
					// do some magic and call reconnect when ready
					pubnub.reconnect();
				} else {
//                    log.error(status);
				}

			}

			@Override
			public void message(PubNub pubnub, final PNMessageResult message)
			{
				Log.d("presence_channel",message.getMessage().toString());
			}

			@Override
			public void presence(PubNub pubnub, PNPresenceEventResult presence) {

				Log.d("presence_channel",presence.getEvent());
			}
		});


	}

	public void unSubscribe()
	{
		if(pubnub2!=null){
			pubnub2.unsubscribe()
					.channels(Arrays.asList(session.getPresenceChannel()))
					.execute();

			pubnub2.stop();
		}

	}



}
