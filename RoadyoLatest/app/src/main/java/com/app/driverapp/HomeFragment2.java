package com.app.driverapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.pojo.AppointmentDetailList;
import com.app.driverapp.pojo.PubnubResponse;
import com.app.driverapp.response.AppointmentData;
import com.app.driverapp.response.AppointmentDetailData;
import com.app.driverapp.response.AppointmentStatusResponse;
import com.app.driverapp.response.MasterStatusResponse;
import com.app.driverapp.response.RespondAppointment;
import com.app.driverapp.response.UpdateAppointMentstatus;
import com.app.driverapp.utility.ConnectionDetector;
import com.app.driverapp.utility.LocationFinder;
import com.app.driverapp.utility.LocationUtil;
import com.app.driverapp.utility.NetworkConnection;
import com.app.driverapp.utility.NetworkNotifier;
import com.app.driverapp.utility.PicassoMarker;
import com.app.driverapp.utility.PublishUtility;
import com.app.driverapp.utility.Scaler;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by embed on 7/5/16.
 */
public class   HomeFragment2 extends Fragment implements GoogleMap.OnMapClickListener,NetworkNotifier
{

    private MainActivity mainActivity;
    private PubNub pubnub;
    private SessionManager session;
    private ProgressDialog mdialog;
    private String driverChannelName;
    private LocationUtil locationUtil;
    private LocationManager manager;
    private IntentFilter filter;
    private BroadcastReceiver receiver;
    private int type;
    private View view;
    private TextView tvTripsToday,tripTodayHeader,tvEarningsTodayHeader,tvLastTripHeader,tvEstimatedNetHeader;
    private TextView tvEarningsToday;
    private TextView tvWallet;
    private TextView network_text;
    private RelativeLayout network_bar;
    private TextView tvLastTripTime;
    private TextView tvLastTripPrice;
    private Button jobStatus;
    private boolean mOnTheJob;
    private ArrayList<AppointmentDetailData> appointmentDetailDatas;
    private AppointmentDetailData appointmentDetailData;
    private AppointmentDetailList appointmentDetailList=new AppointmentDetailList();
    private String listener_channel;
    private String pasChannel;
   // private Intent serviceIntent;
    private boolean isRequested=false;
    private GoogleMap googleMap;
    private LocationFinder newLocationFinder;
    private SimpleDateFormat simpleDateFormat,sdfStandard;


    private double mLatitude;
    private double mLongitude;
    private Marker carMarker;
    private Dialog alertDialog;
    private Button accept_btn;
    private Button reject_btn;
    private TextView textView;
    private int mm;
    private int ss;
    private String resp;
    private TextView mTVBid;
    private TextView mTvPickup;
    private TextView mTvApptTime;
    private TextView mTvApptDate;
    private TextView tvLastTripCarType;
    private CircularProgressBar circularProgressBar;
    private PicassoMarker marker;
    private CountDownTimer mCounter=null;
    private Typeface font,fontBold;
    private TextView mTvDate;
    private Location mCurrentLoc,mPreviousLoc;
    private MediaPlayer mediaPlayer;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity)getActivity();
        pubnub = mainActivity.getPubnubObject();
        session = new SessionManager(getActivity());
        session.setIsHomeFragmentisOpened(true);
        simpleDateFormat=new SimpleDateFormat("dd MMM yyyy h:mm a", Locale.US);
        sdfStandard=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        mdialog = Utility.GetProcessDialog(getActivity());
        driverChannelName = session.getSubscribeChannel();
        locationUtil=new LocationUtil(getActivity(),this);
        manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );

        Utility.printLog("listener_channel = "+listener_channel+"  driverChannelName = "+driverChannelName);

        filter = new IntentFilter();
        filter.addAction("com.embed.anddroidpushntificationdemo11.push");
        filter.addAction("com.app.driverapp.internetStatus");
        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                try
                {
                    Bundle bucket=intent.getExtras();
                    String status = bucket.getString("STATUS");
                    String action = bucket.getString("action");

                    Utility.printLog("CCCCCC Animation action"+action);

                    if("7".equals(action))
                    {
                        type = 1;
                        Utility.printLog("CCCCCC Booking type1 = "+type);

                        Utility.printLog("Animation action found 7");
                        String bid = bucket.getString("bid");
                        if (!(bid.equals(session.getBOOKING_ID())))
                        {
                            //session.setBOOKING_ID(bid);
                            getAppointmentStatus();
                        }
                    }
                    else if("51".equals(action))
                    {
                        type = 2;
                        Utility.printLog("CCCCCC Booking type2 = "+type);
                        String bid = bucket.getString("bid");
                        if (!(bid.equals(session.getBOOKING_ID())))
                        {

                            //session.setBOOKING_ID(bid);
                            getAppointmentStatus();
                        }
                    }
                    else if ("11".equals(action))
                    {
                        String payload = bucket.getString("payload");
                        ErrorMessage(getResources().getString(R.string.messagetitle),payload,false);
                    }
                    else if ("12".equals(action))
                    {
                        Utility.printLog("out side 12"+session.getIsUserRejectedFromAdmin());
                        String payload = bucket.getString("payload");
                        //ErrorMessage(getResources().getString(R.string.messagetitle),payload,false);
                        ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), payload);

                    }

                    if("1".equals(status))
                    {
                        network_bar.setVisibility(View.GONE);
                       // pubnub.reconnect();
                        NotificationManager notificationManager= (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();
                        getAppointmentStatus();
                    }
                    else
                    {
                        if (!Utility.isNetworkAvailable(getActivity()))
                        {
                            network_bar.setVisibility(View.VISIBLE);
                            return;
                        }
                        else if (!NetworkConnection.isConnectedFast(getActivity()))
                        {
                            network_bar.setVisibility(View.VISIBLE);
                            network_text.setText(getResources().getString(R.string.lownetwork));
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Utility.printLog("AAA BroadcastReceiver Exception "+e);
                    e.printStackTrace();
                }
            }
        };
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
        @Override
        public View onCreateView(android.view.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (view != null)
            {
                ViewGroup parent = (ViewGroup) view.getParent();
                if (parent != null)
                    parent.removeView(view);
            }
            try
            {
                view = inflater.inflate(R.layout.homefragment_2, null);

            } catch (InflateException e)
            {
                Utility.printLog("onCreateView  InflateException "+e);
            }

            initLayoutid(view);


            return view;
        }
///////////////////////////////////////////////////////////////////////////////////////////////////

    private void initLayoutid(View view)
    {
        font = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Regular.ttf");
        fontBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Lato-Bold.ttf");

        tvTripsToday= (TextView) view.findViewById(R.id.tvTripsToday);
        tripTodayHeader= (TextView) view.findViewById(R.id.tripTodayHeader);
        tvEarningsToday= (TextView) view.findViewById(R.id.tvEarningsToday);
        tvEarningsTodayHeader= (TextView) view.findViewById(R.id.tvEarningsTodayHeader);
        tvLastTripHeader= (TextView) view.findViewById(R.id.tvLastTripHeader);
        tvEstimatedNetHeader= (TextView) view.findViewById(R.id.tvEstimatedNetHeader);
        tvWallet= (TextView) view.findViewById(R.id.tvWallet);

        tvLastTripTime= (TextView) view.findViewById(R.id.tvLastTripTime);
        tvLastTripPrice= (TextView) view.findViewById(R.id.tvLastTripPrice);
        tvLastTripCarType= (TextView) view.findViewById(R.id.tvLastTripCarType);

        network_text = (TextView)view.findViewById(R.id.network_text);
        network_bar = (RelativeLayout)view.findViewById(R.id.network_bar);

        tvTripsToday.setTypeface(font);
        tvEarningsToday.setTypeface(font);
        tvLastTripTime.setTypeface(font);
        tvLastTripPrice.setTypeface(font);
        tvWallet.setTypeface(font);
        tvLastTripCarType.setTypeface(font);

        tripTodayHeader.setTypeface(fontBold);
        tvEarningsTodayHeader.setTypeface(fontBold);
        tvLastTripHeader.setTypeface(fontBold);
        tvEstimatedNetHeader.setTypeface(fontBold);

        jobStatus = (Button) view.findViewById(R.id.btnOnTheJob);
        jobStatus.setTypeface(fontBold);
        jobStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(mOnTheJob)
                {
                    getMasterUpdateStatus(4);
                }
                else
                {
                    getMasterUpdateStatus(3);
                }
            }
        });

    }

///////////////////////////////////////////////////////////////////////////////////////////////////

    private void subscribe(final String channel)
    {
        try{
            pubnub.subscribe()
                    .withPresence()
                    .channels(Arrays.asList(channel)).execute(); // subscribe to channels.execute();




        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
                // handle any status
                if(status!=null){
                    if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                        // internet got lost, do some magic and call reconnect when ready
                        if(Utility.isNetworkAvailable(getActivity()))
                        {
                            Utility.printLog("PK pubnub reconnect called");
                            pubnub.reconnect();
                        }
                        // pubnub.reconnect();
                    } else if (status.getCategory() == PNStatusCategory.PNTimeoutCategory) {
                        // do some magic and call reconnect when ready
                        pubnub.reconnect();
                    } else {
//                    log.error(status);
                    }
                }


            }

            @Override
            public void message(PubNub pubnub, final PNMessageResult message)
            {
                // handle incoming messages
//                pubnub.hereNow();
               // Log.d("messages123",message.getMessage().get(0)+"\t"+message.getMessage().get(1)+"\t"+message.getMessage().get(2));
                mainActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Utility.printLog("AAA successCallback message="+message.getMessage());

                        PubnubResponse pubnubResponse;
                        Gson gson = new Gson();
                        pubnubResponse = gson.fromJson(String.valueOf(message.getMessage()), PubnubResponse.class);
                        Utility.printLog("AAA inside successCallback bool = "+MainActivity.isResponse);
                        Utility.printLog("AAA successCallback  if ");
                        try {
                            if ("11".equals(pubnubResponse.getA()) || "51".equals(pubnubResponse.getNt())) {
                                //String aptDate = pubnubResponse.getDt();
                                //String email = pubnubResponse.getE();
                                String bid = pubnubResponse.getBid();

                              /*  if (!(bid.equals(session.getBOOKING_ID())))
                                {*/
                                    //session.setBOOKING_ID(bid);
                                    session.setFlagNewBooking(false);
                                    Utility.printLog("Animation Action successCallback  try if");
                                    MainActivity.isResponse = false;
                                    Utility.printLog("sendResponsePubnub :" + pubnubResponse);
//                                    session.setPASSENGER_EMAIL("");

                                    if ("51".equals(pubnubResponse.getNt())){
                                        type = 2;
                                    }
                                    else
                                    {
                                        type = 1;
                                    }
                                    getAppointmentStatus();
                               /* } else if ("4".equals(pubnubResponse.getA())) {
                                    Utility.printLog("successCallback  try if");
                                    session.setPasChannel("");
                                }*/
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });

            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
                // handle incoming presence data
//                pubnub.hereNow().channels(Arrays.asList("new_pubnub_channel"));
                Log.d("messages123",presence.getEvent());
            }
        });

        }catch (Exception e){
            e.printStackTrace();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
private void getMasterUpdateStatus(int masterStatus)
{
    Utility utility=new Utility();
    String sessionToken=session.getSessionToken();
    String deviceid=Utility.getDeviceId(getActivity());
    String currentDate=utility.getCurrentGmtTime();

    final String mparams[]={sessionToken,deviceid,""+masterStatus,currentDate};
    if(mOnTheJob){
        mdialog.setMessage("Going offline...");
    }
    else {
        mdialog.setMessage("Going online...");
    }
    //mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
    mdialog.show();
    mdialog.setCancelable(false);
    RequestQueue queue = Volley.newRequestQueue(getActivity());
    String url = VariableConstants.getMasterStatus_url;
    StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofMasterStatus,errorListenerofMasreStatus )
    {
        @Override
        protected Map<String, String> getParams()
        {
            Map<String, String>  params = new HashMap<String, String>();
            params.put("ent_sess_token", mparams[0]);
            params.put("ent_dev_id", mparams[1]);
            params.put("ent_status", mparams[2]);
            params.put("ent_date_time", mparams[3]);
            Utility.printLog("getMasterStatus  request "+params);
            return params;
        }
    };
    int socketTimeout = 60000;//60 seconds - change to what you want
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postRequest.setRetryPolicy(policy);
    queue.add(postRequest);
}

    Response.Listener<String> responseListenerofMasterStatus=new Response.Listener<String>()
    {
        @Override
        public void onResponse(String response)
        {
            try
            {
                Utility.printLog("getMasterStatus response "+response);
                MasterStatusResponse masterStatusResponse;
                Gson gson = new Gson();
                masterStatusResponse = gson.fromJson(response, MasterStatusResponse.class);

                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }

                if (masterStatusResponse.getErrFlag() == 0 && masterStatusResponse.getErrNum() == 69)
                {
                    Toast.makeText(getActivity(), masterStatusResponse.getErrMsg(), Toast.LENGTH_SHORT).show();


                    if(mOnTheJob)
                    {
                        session.setIsOnTheJob(false);
                        mainActivity.unSubscribe();
                        getActivity().stopService(new Intent(getActivity(),MyService.class));
                        jobStatus.setBackgroundColor(getResources().getColor(R.color.onThejob));
                        jobStatus.setText(getResources().getString(R.string.onTheJob));
//                        session.setIsOnButtonClicked(true);
                        mOnTheJob=false;
                    }
                    else
                    {
                        session.setIsOnTheJob(true);
                        mainActivity.subscribe(session.getPresenceChannel());
                        getActivity().startService(new Intent(getActivity(),MyService.class));
                        jobStatus.setBackgroundColor(getResources().getColor(R.color.offTheJob));
                        jobStatus.setText(getResources().getString(R.string.offTheJob));
//                        session.setIsOnButtonClicked(false);
                        mOnTheJob = true;
                    }

                }
                if (masterStatusResponse.getErrFlag() == 1 && masterStatusResponse.getErrNum() == 99)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), masterStatusResponse.getErrMsg());
                }
                if (masterStatusResponse.getErrFlag() == 1 && masterStatusResponse.getErrNum() == 101)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), masterStatusResponse.getErrMsg());
                }
                if (masterStatusResponse.getErrFlag() == 1 && masterStatusResponse.getErrNum() == 125)
                {
                    Toast.makeText(getActivity(), masterStatusResponse.getErrMsg(), Toast.LENGTH_SHORT).show();
                    //getAppointmentStatus();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }
                //ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
            }

        }
    };

    Response.ErrorListener errorListenerofMasreStatus=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            if (mdialog!=null)
            {
                mdialog.dismiss();
                mdialog.cancel();
            }

        }
    };

////////////////////////////////////////////////////////////////////////////////////////////////////


    private void getAppointmentStatus()
    {
        Utility utility=new Utility();
        ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet())
        {
            String deviceid=Utility.getDeviceId(getActivity());
            String curenttime=utility.getCurrentGmtTime();
            String sessiontoken = session.getSessionToken();
            String apttime = session.getAPPT_DATE();
            final String mparams[]={sessiontoken,deviceid ,curenttime};

            mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
            mdialog.show();
            mdialog.setCancelable(false);
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String  url = VariableConstants.getAppointmenttatus_url;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofApptStatus,errorListenerApptStatus )
            {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("ent_sess_token",mparams[0]);
                    params.put("ent_dev_id",mparams[1]);
                    params.put("ent_user_type","1");
                    params.put("ent_date_time", mparams[2]);
                    params.put("ent_appnt_dt", "");
                    Utility.printLog("paramsRequest"+params);
                    return params;
                }
            };
            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            queue.add(postRequest);
        }
    }

    Response.Listener<String> responseListenerofApptStatus=new Response.Listener<String>()
    {
        @Override
        public void onResponse(String response)
        {
            try
            {
                Utility.printLog("AppointmentStatusResponse"+response);
                AppointmentStatusResponse appointmentStatusResponse;
                Gson gson = new Gson();
                appointmentStatusResponse = gson.fromJson(response, AppointmentStatusResponse.class);
                if (appointmentStatusResponse.getErrFlag() == 0 && appointmentStatusResponse.getErrNum() == 21)
                {
                   // session.setIsInBooking(true);
                    appointmentDetailDatas = appointmentStatusResponse.getData();
                    appointmentDetailList.setAppointmentDetailData(appointmentDetailDatas.get(0));
                    Utility.printLog("AAAA " + appointmentDetailDatas.get(0).getStatus());


                    if ("1".equals(appointmentDetailDatas.get(0).getStatus()))
                    {
                        session.setIsOnOff(true);
                        getAppointmentDetails(appointmentDetailDatas.get(0).getEmail(), appointmentDetailDatas.get(0).getApptDt());
                    }
                    else if ("6".equals(appointmentDetailDatas.get(0).getStatus()))
                    {
                        if (mdialog!=null)
                        {
                            mdialog.dismiss();
                            mdialog.cancel();
                        }
                        Utility.printLog("Inside appointmentDetailDatas");
                        Intent intent=new Intent(getActivity(), IHaveArrivedActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putSerializable(VariableConstants.APPOINTMENT, appointmentDetailList);
                        Utility.printLog("III Main appointmentDetailList " + appointmentDetailList);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    else if ("7".equals(appointmentDetailDatas.get(0).getStatus()))
                    {
                        if (mdialog!=null)
                        {
                            mdialog.dismiss();
                            mdialog.cancel();
                        }
                        Intent intent=new Intent(getActivity(), BeginJourneyActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putSerializable(VariableConstants.APPOINTMENT,appointmentDetailList);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if ("8".equals(appointmentDetailDatas.get(0).getStatus()))
                    {
                        if (mdialog!=null)
                        {
                            mdialog.dismiss();
                            mdialog.cancel();
                        }
                        /*if (session.getFlagForStatusDropped())
                        {
                            Intent intent=new Intent(getActivity(), JourneyDetailsActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable(VariableConstants.APPOINTMENT,appointmentDetailList);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            getActivity().finish();
                        }
                        else
                        {*/
                            Intent intent=new Intent(getActivity(), PassengerDroppedActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable(VariableConstants.APPOINTMENT,appointmentDetailList);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            getActivity().finish();
                        //}

                    }
                }
                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 96)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }

                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 79)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }
                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 6)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }
                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 7)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }
                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 101)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }
                else if (appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 99)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());
                }
                else if(appointmentStatusResponse.getErrNum()==6 || appointmentStatusResponse.getErrNum()==7 ||
                        appointmentStatusResponse.getErrNum()==94 || appointmentStatusResponse.getErrNum()==96)
                {
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentStatusResponse.getErrMsg());

                }
                else if(appointmentStatusResponse.getErrFlag() == 1 && appointmentStatusResponse.getErrNum() == 49 && "3".equals(appointmentStatusResponse.getMasStatus()) )
                {
                    session.setIsInBooking(false);
                    session.setAppointmentStatus(4);
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    session.setIsOnOff(true);
                    Utility.printLog("Status1 main= " + session.IsOnOff());
                }
                else {
                    session.setIsInBooking(false);
                    if (mdialog!=null)
                    {
                        mdialog.dismiss();
                        mdialog.cancel();
                    }
                    session.setIsOnOff(false);
                    Utility.printLog("Status2 main= " + session.IsOnOff());
                }

                if (appointmentStatusResponse.getMasStatus().equals("4")) {
                    getActivity().stopService(new Intent(getActivity(),MyService.class));
                    session.setIsOnTheJob(false);
                    jobStatus.setBackgroundColor(getResources().getColor(R.color.onThejob));
                    jobStatus.setText(getResources().getString(R.string.onTheJob));
//                    session.setIsOnButtonClicked(true);
                    mOnTheJob=false;

                }
                else
                {
                    getActivity().startService(new Intent(getActivity(),MyService.class));
                    session.setIsOnTheJob(true);
                    jobStatus.setBackgroundColor(getResources().getColor(R.color.offTheJob));
                    jobStatus.setText(getResources().getString(R.string.offTheJob));
//                    session.setIsOnButtonClicked(false);
                    mOnTheJob = true;
                }

                if(!"".equals(appointmentStatusResponse.getTripsToday()))
                {
                    tvTripsToday.setText(appointmentStatusResponse.getTripsToday());
                }
                if(!"".equals(appointmentStatusResponse.getEarningsToday()))
                {
                    tvEarningsToday.setText("$ "+appointmentStatusResponse.getEarningsToday());
                }
                if(!"".equals(appointmentStatusResponse.getLastTripPrice()))
                {
                    tvLastTripPrice.setText("$ "+appointmentStatusResponse.getLastTripPrice());
                }
                if(!"".equals(appointmentStatusResponse.getLastTripTime()))
                {
                    Date date=sdfStandard.parse(appointmentStatusResponse.getLastTripTime());
                    String mdate=simpleDateFormat.format(date);
                    tvLastTripTime.setText(mdate);
                }
                else
                {
                    tvLastTripTime.setText("NA");
                }
                if(!"".equals(appointmentStatusResponse.getCarType()))
                {
                    tvLastTripCarType.setText(appointmentStatusResponse.getCarType());
                }
                if(!"".equals(appointmentStatusResponse.getWallet()))
                {
                    tvWallet.setText("$ "+appointmentStatusResponse.getWallet());
                }
/*

                if(!"".equals(appointmentStatusResponse.getWallet())&&!"".equals(appointmentStatusResponse.getBalanceLimit()))
                {
                    Double balance=Double.parseDouble(appointmentStatusResponse.getWallet());
                    Double balanceLimit=Double.parseDouble(appointmentStatusResponse.getBalanceLimit());
                    if(balance>balanceLimit)
                    {
                        jobStatus.setBackgroundColor(getResources().getColor(R.color.offTheJob));
                        jobStatus.setText(getResources().getString(R.string.offTheJob));
                        session.setIsOnButtonClicked(false);
                        mOnTheJob = true;
                        jobStatus.setClickable(true);
                    }
                    else
                    {
                        jobStatus.setBackgroundColor(getResources().getColor(R.color.onThejob));
                        jobStatus.setText(getResources().getString(R.string.onTheJob));
                        session.setIsOnButtonClicked(true);
                        mOnTheJob=false;
                        jobStatus.setClickable(false);
                    }

                }
*/

            }
            catch (Exception e)
            {
                Utility.printLog("Exception = "+e);
                e.printStackTrace();
                //ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
            }
        }
    };
    Response.ErrorListener errorListenerApptStatus=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            try {
                //Toast.makeText(getApplicationContext(), ""+error, Toast.LENGTH_SHORT).show();
                Utility.printLog("AAAAAAAAAVolleyError" + error);
                if (mdialog != null) {
                    mdialog.dismiss();
                    mdialog.cancel();
                }
                ErrorMessage(getResources().getString(R.string.messagetitle), getResources().getString(R.string.nonetwork), false);
            }catch (Exception e){
                e.printStackTrace();
            }
            //Utility.ShowAlert(getResources().getString(R.string.network), MainActivity.this);
        }
    };

////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method for getting appointment detail.
     * @param email
     * @param aptDateTime
     */
    private void getAppointmentDetails(String email,String aptDateTime)
    {
        // animation fo accept or reject with timer
        //animatedUiHomePage(30000);
        Utility.printLog("Animation action email"+email,"Animation action Date Time"+aptDateTime);
        SessionManager sessionManager=new SessionManager(getActivity());
        Utility utility=new Utility();
        String sessionToken=sessionManager.getSessionToken();
        String deviceid=Utility.getDeviceId(getActivity());
        String currentDate=utility.getCurrentGmtTime();

        final String mparams[]={sessionToken,deviceid,email,aptDateTime,currentDate};
        //final ProgressDialog mdialog;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = VariableConstants.getAppointmentDetails_url;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofAppointment,errorListener1 )
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("ent_sess_token", mparams[0]);
                params.put("ent_dev_id", mparams[1]);
                params.put("ent_email", mparams[2]);
                params.put("ent_appnt_dt", mparams[3]);
                params.put("ent_date_time", mparams[4]);
                params.put("ent_user_type", "1");
                Utility.printLog("getAppointmentDetails  request "+params);
                return params;
            }
        };
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }


    Response.Listener<String> responseListenerofAppointment=new Response.Listener<String>()
    {
        @Override
        public void onResponse(String response)
        {
            if (mdialog!=null)
            {
                mdialog.dismiss();
                mdialog.cancel();
            }
            try
            {
                Utility.printLog("getAppointmentDetails  response "+response);
                AppointmentData appointmentData;
                Gson gson = new Gson();
                appointmentData=gson.fromJson(response, AppointmentData.class);

                if (appointmentData.getErrFlag()==0 && appointmentData.getErrNum() == 21)
                {
                    Utility.printLog("Animation action Response Came with 0 and 21");
//                    session.setAPT_DATE("");
//                    session.setPASSENGER_EMAIL("");
                    appointmentDetailData  = appointmentData.getData();
                    appointmentDetailList = new AppointmentDetailList();
                    appointmentDetailList.setAppointmentDetailData(appointmentDetailData);
                    // 21 -> (0) Got the details!
                    /*String pickuplocstr = appointmentDetailData.getAddr1();
                    //String dropofflocstr = appointmentDetailData.getDropAddr2();
                    String pickupdisstr = appointmentDetailData.getApptDis();
                    //String dropoffstr = appointmentDetailData.getDis();
                    String bookingIdstr = appointmentDetailData.getBid();*/
                    pasChannel = appointmentDetailData.getPasChn();
                    session.setPasChannel(pasChannel);
                    session.setBOOKING_ID(appointmentDetailData.getBid());
//                    session.setBookingid(appointmentDetailData.getBid());
                    session.setAPPT_DATE(appointmentDetailData.getApptDt());
                    session.setMobile(appointmentDetailData.getMobile());
                    session.setDate(appointmentDetailData.getApptDt());
                    session.setPASSENGER_EMAIL(appointmentDetailData.getEmail());
                   /* pickUpLoacaton.setText(""+pickuplocstr);
                    pickupDistanse.setText(""+pickupdisstr);
                    if (!"".equals(appointmentDetailData.getDropAddr1()))
                    {
                        drop_layout.setVisibility(View.VISIBLE);
                        String dropofflocstr = appointmentDetailData.getDropAddr1();
                        String dropoffstr = appointmentDetailData.getDis();
                        dropOffLocation.setText(""+dropofflocstr);
                        dropoffDistanse.setText(""+dropoffstr);
                    }
                    else
                    {
                        drop_layout.setVisibility(View.GONE);
                    }


                    booking_id.setText(""+bookingIdstr);


                    datetimetext.setText(appointmentDetailData.getApptDt());*/
                    int remainingSeconds=Integer.parseInt(appointmentDetailData.getExpireSec().toString());
                    //remainingSeconds=remainingSeconds*1000;
                    //animatedUiHomePage(remainingSeconds);

                    if(!isRequested){
                        mRequestPopUp(remainingSeconds,appointmentDetailData);
                    }


                    NotificationManager nMgr = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                    nMgr.cancelAll();

                }
                else if (appointmentData.getErrFlag()==1 && appointmentData.getErrNum()==3)
                {
                    // 3 -> (1) Error occurred while processing your request.
                    ErrorMessage(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg(),false);
                }
                else if (appointmentData.getErrFlag()==1 && appointmentData.getErrNum()== 72 )
                {
                    ErrorMessageForExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }
                else if (appointmentData.getErrFlag()==1 && appointmentData.getErrNum()== 62 )
                {
                    // 3 -> (1) Error occurred while booking not found.
                    ErrorMessage(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg(),true);
                }
                else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum() == 4)
                {
                    ErrorMessageForExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }
                else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum()==9)
                {
                    ErrorMessageForExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }
                else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum()==99)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }
                else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum()==101)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }
                else if(appointmentData.getErrNum()==6|| appointmentData.getErrNum()==7 ||
                        appointmentData.getErrNum()==94 || appointmentData.getErrNum()==96)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg());
                }

            }
            catch (Exception e)
            {
                Utility.printLog("getAppointmentDetailException = "+e);
                e.printStackTrace();
                //ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
            }
        }
    };
///////////////////////////////////////////////////////////////////////////////////////////////////
        @Override
        public void onResume()
        {
            super.onResume();


            NotificationManager nMgr = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            nMgr.cancelAll();

            listener_channel = session.getListerChannel();
            if(locationUtil!=null&&!locationUtil.isGoogleAPIConnected()){
                locationUtil.connectGoogleApiClient();
            }

           /* if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            {
                buildAlertMessageNoGps();
            }*/

           // getActivity().stopService(serviceIntent);

            subscribe(listener_channel);
            session.setIsHomeFragmentisOpened(true);
            SupportMapFragment fm = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
            // Getting GoogleMap object from the fragment


            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);
            double size[]= Scaler.getScalingFactor(getActivity());
            int value=(int)Math.round(70* size[0]);
            googleMap.setPadding(0,value,0,0);

           // newLocationFinder = new LocationFinder();
           // newLocationFinder.getLocation(getActivity(), mLocationResult);
            googleMap.setOnMapClickListener(this);
            LatLng latLng = new LatLng(session.getDriverCurrentLat(), session.getDriverCurrentLongi());



            try{
                marker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng).title("First Point")));
                Utility.printLog("Picasso : "+session.getVehicleTypeUrl());
                if(!session.getVehicleTypeUrl().equals(""))
                {
                    String url=VariableConstants.ImageUrl+session.getVehicleTypeUrl();
                    Picasso.with(getActivity()).load(url).resize(75,150).into(marker);
                }
                else
                {
                    Picasso.with(getActivity()).load(R.drawable.home_caricon_black).into(marker);
                }
            }
            catch (Exception e)
            {
                Log.e("Picasso","caught an exception");
            }
            setCarMarker(latLng);
            /*googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            try {
                carMarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_caricon_black)));

            }catch (Exception e){e.printStackTrace();}*/

            Utility.printLog("Animation action oNResume Called");
            session.setIsHomeFragmentisOpened(true);
//            session.setIsFlagForOther(false);


            if (receiver!=null)
            {
                getActivity().registerReceiver(receiver, filter);
            }
            else
            {
                Utility.printLog("Animation actio receiver Null");
            }


            getAppointmentStatus();


            if (session.isCancelPushFlag())
            {
                session.setCancelPushFlag(false);
//                session.setBookingid("");
                ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.cancelbooking),true);
            }

            if (session.isFlagForPayment())
            {
                session.setFlagForPayment(false);
                ErrorMessage(getResources().getString(R.string.messagetitle), session.getPayload(), false);
            }
        }

////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method for showing error message
     * @param title
     * @param message
     * @param flageforSwithchActivity
     */
    private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
    {
        session.setCancelPushFlag(false);
        session.setFlagForPayment(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        if (flageforSwithchActivity)
                        {
                            Intent intent = new Intent(getActivity(),MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                            dialog.dismiss();
                        }
                        else
                        {
                            // only show message
                            dialog.dismiss();
                        }

                    }
                });
        AlertDialog	 alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void ErrorMessageForInvalidOrExpired(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        SessionManager sessionManager=new SessionManager(getActivity());
                        sessionManager.logoutUser();
                        dialog.dismiss();
                        //getActivity().stopService(serviceIntent);
                        Intent intent=new Intent(getActivity(), SplashActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });

        AlertDialog	 alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }
 //////////////////////////////////////////////////////////////////////////////////////////////////

    private void ErrorMessageForExpired(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        /*if (countDownTimer != null)
                        {
                            MainActivity.isResponse = true;
                            countDownTimer.cancel();
                            countDownTimer = null;
                            progressBarLayout.setVisibility(View.GONE);
                            locLinear.setVisibility(View.GONE);
                            bottomLayout.setVisibility(View.GONE);
                        }*/
                        dialog.dismiss();
                    }
                });

        AlertDialog	 alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
private void sendNotificationTOPassenger(final int responsecode)
{
    Utility utility=new Utility();
    ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
    if (connectionDetector.isConnectingToInternet())
    {
        String deviceid=Utility.getDeviceId(getActivity());
        String curenttime=utility.getCurrentGmtTime();
        SessionManager sessionManager=new SessionManager(getActivity());
			/*String passengerEmailid = emailpassenger;
			String appointdatetime = currentDateTime;*/
        String passengerEmailid = appointmentDetailDatas.get(0).getEmail();
        String appointdatetime = appointmentDetailDatas.get(0).getApptDt();
        String sessiontoken = sessionManager.getSessionToken();
        String notes="testing";
        final String mparams[]={sessiontoken,deviceid,passengerEmailid,appointdatetime,""+responsecode ,notes,curenttime/*currentdata[0]*/};

        mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
        mdialog.show();
        mdialog.setCancelable(false);
        RequestQueue queue = Volley.newRequestQueue(getActivity());  // this = context
        String  url = VariableConstants.getAppointmentstatusUpdate_url;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofSendNotification,errorListener1 )
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("ent_sess_token",mparams[0]);
                params.put("ent_dev_id",mparams[1]);

                params.put("ent_pas_email", mparams[2]);
                params.put("ent_appnt_dt",mparams[3]);

                params.put("ent_response", mparams[4]);
                params.put("ent_amount", "");
                params.put("ent_doc_remarks", mparams[5]);
                params.put("ent_date_time", mparams[6]);

                Utility.printLog("AAA paramsRequest"+params);
                return params;
            }
        };
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }
}
    Response.Listener<String> responseListenerofSendNotification=new Response.Listener<String>()
    {
        @Override
        public void onResponse(String response)
        {
            try
            {
                Utility.printLog("sendNotificationResponse"+response);
                UpdateAppointMentstatus appointMentstatus;
                Gson gson = new Gson();
                appointMentstatus = gson.fromJson(response, UpdateAppointMentstatus.class);
                Utility.printLog("AAA sendNotificationTOPassenger sendResponse"+appointMentstatus);

                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }
                //  1 -> (1) Mandatory field missing
                if (appointMentstatus.getErrFlag()==0 && appointMentstatus.getErrNum() == 57)
                {
                    // 57 -> (0) Status updated.
                    SessionManager sessionManager=new SessionManager(getActivity());
                    session.setIsInBooking(true);

                    MainActivity.isResponse = true;
//                    sessionManager.setIsAppointmentAccept(true);
                    sessionManager.setIhavarrived(true);
//                    sessionManager.setAppiontmentStatus(6);
//                    sessionManager.setAPT_DATE("");
//                    sessionManager.setPASSENGER_EMAIL("");
                    Intent intent=new Intent(getActivity(), IHaveArrivedActivity.class);
                    Bundle bundle=new Bundle();
                    Utility.printLog("Latitude  = "+sessionManager.getDriverCurrentLat(),"Longitude = "+sessionManager.getDriverCurrentLongi());
                    for (int i = 0; i < 5; i++)
                    {
                        publishLocation(sessionManager.getDriverCurrentLat(),sessionManager.getDriverCurrentLongi());
                    }
                    bundle.putSerializable(VariableConstants.APPOINTMENT, appointmentDetailList);
                    intent.putExtras(bundle);
                    Utility.printLog("Rahul SendNotification "+bundle);
                    startActivity(intent);
                    getActivity().finish();
                }

                else if (appointMentstatus.getErrFlag()==1 && appointMentstatus.getErrNum() == 56)
                {
                    // 56 -> (1) Invalid status, cannot update.
                    ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
                }
                else if (appointMentstatus.getErrFlag()==1 && appointMentstatus.getErrNum() == 41)
                {
                    // 41 -> (1) Passenger canceled.
                    ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
                }

                else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==3)
                {
                    //           	   				3 -> (1) Error occurred while processing your request.
                    ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
                }
                else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==1)
                {
                    //     1 -> (1) Mandatory field missing
                    ErrorMessage(getResources().getString(R.string.messagetitle), appointMentstatus.getErrMsg(), false);
                }
                else if(appointMentstatus.getErrNum()==6|| appointMentstatus.getErrNum()==7 ||
                        appointMentstatus.getErrNum()==94 || appointMentstatus.getErrNum()==96)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointMentstatus.getErrMsg());
                }
            }
            catch (Exception e)
            {
                Utility.printLog("Exception"+e);
                //ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
            }
        }
    };

    Response.ErrorListener errorListener1=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            if (mdialog!=null)
            {
                mdialog.dismiss();
                mdialog.cancel();
            }
            ErrorMessage(getResources().getString(R.string.messagetitle), getResources().getString(R.string.nonetwork), false);
            //Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
            Utility.printLog("Animation action Response Didnt Came Error");
        }
    };
///////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Method for publish current location to passenger.
     * @param latitude
     * @param longitude
     */
    public void publishLocation(double latitude,double longitude)
    {
       // String message ;
        SessionManager sessionManager = new SessionManager(mainActivity);
        String subscribChannel=sessionManager.getSubscribeChannel();
        String phone = sessionManager.getMobile();

       // message="{\"a\" :\""+6+"\", \"e_id\" :\""+sessionManager.getUserEmailid()+"\", \"lt\" :"+latitude+", \"lg\" :"+longitude+", \"ph\" :\""+phone+"\",\"dt\" :\""+sessionManager.getDate()+"\",\"bid\" :\""+sessionManager.getBOOKING_ID()+"\",\"chn\" :\""+subscribChannel+"\"}";
        sessionManager.setAppointmentStatus(6);
        HashMap<String,String> hashMap=new HashMap();
        hashMap.put("a","6");
        hashMap.put("e_id",sessionManager.getUserEmailid());
        hashMap.put("ph",phone);
        hashMap.put("lt",""+latitude);
        hashMap.put("lg",""+longitude);
        hashMap.put("dt",sessionManager.getDate());
        hashMap.put("bid",sessionManager.getBOOKING_ID());
        hashMap.put("chn",subscribChannel);



        Utility.printLog("Publish Location = "+hashMap.toString());
        if (pasChannel != null)
        {
            Utility.printLog("Publish  pasChannel= "+pasChannel);
            PublishUtility.publish(pasChannel, hashMap, pubnub);
        }
        else
        {
            ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.passengercancelled),false);
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method for calling service of rejecting Appointment
     * @param responsecode
     */
    private void getRejectStatus(final int responsecode)
    {
        Utility utility=new Utility();

        ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet())
        {
            String deviceid=Utility.getDeviceId(getActivity());
            String curenttime=utility.getCurrentGmtTime();
            SessionManager sessionManager=new SessionManager(getActivity());
			/*String passengerEmailid = emailpassenger;
			String appointdatetime = currentDateTime;*/
            String passengerEmailid = appointmentDetailDatas.get(0).getEmail();
            String appointdatetime = appointmentDetailDatas.get(0).getApptDt();
            String sessiontoken = sessionManager.getSessionToken();
            String notes="testing";
            final String mparams[]={sessiontoken,deviceid,passengerEmailid,appointdatetime,""+responsecode ,notes,curenttime/*currentdata[0]*/};

            mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
            mdialog.show();
            mdialog.setCancelable(false);
            RequestQueue queue = Volley.newRequestQueue(getActivity());  // this = context
            String  url = VariableConstants.getRejectstatusUpdate_url;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofRejectNotification,errorListener2 )
            {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("ent_sess_token",mparams[0]);
                    params.put("ent_dev_id",mparams[1]);

                    params.put("ent_pas_email", mparams[2]);
                    params.put("ent_appnt_dt",mparams[3]);

                    params.put("ent_response", mparams[4]);
                    params.put("ent_book_type", mparams[5]);
                    params.put("ent_date_time", mparams[6]);

                    Utility.printLog("paramsRequest"+params);
                    return params;
                }
            };
            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            queue.add(postRequest);
        }
    }

    Response.Listener<String> responseListenerofRejectNotification=new Response.Listener<String>()
    {

        @Override
        public void onResponse(String response)
        {
            try
            {
                RespondAppointment respondAppointment = null;
                Gson gson = new Gson();
                respondAppointment=gson.fromJson(response, RespondAppointment.class);
               /* if(mCounter!=null){
                    circularProgressBar.setProgress(0);//to set timer to zero
                    mCounter.onFinish();
                    mCounter=null;
                }*/


                Utility.printLog("Reject "+response);



                if (mdialog!=null)
                {
                    mdialog.dismiss();
                    mdialog.cancel();
                }

                if (respondAppointment.getErrFlag()==0 && respondAppointment.getErrNum()==40)
                {
                    // 40 -> (0) Appointment updated successfully!
                    ///Intent intent=new Intent(getActivity(), MainActivity.class);
                    MainActivity.isResponse = true;
//                    session.setAPT_DATE("");
//                    session.setPASSENGER_EMAIL("");

                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    //startActivity(intent);
                    //getActivity().finish();
                }

                else if (respondAppointment.getErrFlag()==1 && respondAppointment.getErrNum()==102)
                {
                    // 3 -> (1) Error occurred while processing your request.
                    ErrorMessageReject(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg());
                }

                else if (respondAppointment.getErrFlag()==1 && respondAppointment.getErrNum()==3)
                {
                    // 3 -> (1) Error occurred while processing your request.
                    ErrorMessageReject(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg());
                }
                else if (respondAppointment.getErrFlag()==1 && respondAppointment.getErrNum()==77)
                {
                    // 3 -> (1) Error occurred while processing your request.
                    ErrorMessage(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg(),false);
                }

                else if (respondAppointment.getErrFlag()==0&&respondAppointment.getErrNum()==41)
                {
                    // 41 -> (1) Sorry, passenger have cancelled this appointment.
                    ErrorMessage(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg(),false);
                }
                else if (respondAppointment.getErrFlag()==1&&respondAppointment.getErrNum()==30)
                {
                    //30 -> (1) No appointments on this date.
                    ErrorMessage(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg(),false);
                }

                else if (respondAppointment.getErrFlag()==1&&respondAppointment.getErrNum()==1)
                {
                    //1 -> (1) Mandatory field missing
                    ErrorMessage(getResources().getString(R.string.messagetitle), respondAppointment.getErrMsg(), false);
                }
                else if(respondAppointment.getErrNum()==6|| respondAppointment.getErrNum()==7 ||
                        respondAppointment.getErrNum()==94 || respondAppointment.getErrNum()==96)
                {
                    ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),respondAppointment.getErrMsg());
                }
            }
            catch (Exception e)
            {
                Utility.printLog("Reject appointment response =  "+e);
            }

        }
    };

    Response.ErrorListener errorListener2=new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            if (mdialog!=null)
            {
                mdialog.dismiss();
                mdialog.cancel();
            }
        }
    };

////////////////////////////////////////////////////////////////////////////////////////////////////
        private void ErrorMessageReject(String title,String message)
        {
            session.setCancelPushFlag(false);
            session.setFlagForPayment(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(title);
            builder.setMessage(message);

            builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
//                            session.setAPT_DATE("");
//                            session.setPASSENGER_EMAIL("");
                            dialog.dismiss();
                        }
                    });
            AlertDialog	 alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
/*


    LocationFinder.LocationResult mLocationResult = new LocationFinder.LocationResult()
    {
        public void gotLocation(final double latitude, final double longitude)
        {
            if (latitude == 0.0 || longitude == 0.0)
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        showSettingsAlert();
                    }
                });
                return;
            }
            else
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            mLatitude = latitude;
                            mLongitude = longitude;
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude,mLongitude), 16.0f));
                        }
                        catch (Exception e)
                        {
                            Utility.printLog("Exception = "+e);
                        }
                    }
                });
            }
        }
    };
*/

////////////////////////////////////////////////////////////////////////////////////////////////////

    /*private void showSettingsAlert()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(getResources().getString(R.string.gps));
        alertDialog.setMessage(getResources().getString(R.string.gpsdisable));
        alertDialog.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancelbutton), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                getActivity().finish();
            }
        });
        alertDialog.show();
    }*/
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void updatedInfo(String info) {

    }

    @Override
    public void locationUpdates(Location location) {
        Utility.printLog("Location called lat:"+location.getLatitude()+" Long:"+location.getLongitude());
        if(marker!=null){
            setCarMarker(location);
        }
    }

    @Override
    public void locationFailed(String message) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onPause()
    {
        super.onPause();
        SessionManager sessionManager=new SessionManager(getActivity());
        sessionManager.setIsHomeFragmentisOpened(false);
        getActivity().unregisterReceiver(receiver);
        try{
            locationUtil.disconnectGoogleApiClient();
            if(pubnub!=null){
                //pubnub.unsubscribe(listener_channel);
                pubnub.unsubscribe()
                        .channels(Arrays.asList(listener_channel))
                        .execute();
            }
            if(mdialog != null){
                mdialog.cancel();
                mdialog.dismiss();
            }
            //getActivity().moveTaskToBack(true);
            if (marker != null)
            {
                marker.getmMarker().remove();
            }
            if(googleMap!=null){
                googleMap.clear();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onStop()
    {
        super.onStop();

        //getActivity().startService(serviceIntent);
    }
///////////////////////////////////////////////////////////////////////////////////////////////////

   /* private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setTitle("Location Service Disabled:")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        dialog.dismiss();
                    }
                });
				*//*.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						dialog.cancel();
					}
				});*//*
        final AlertDialog alert = builder.create();
        alert.show();
    }*/
/////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mRequestPopUp(int i, AppointmentDetailData appointmentDetailData)
    {
        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.taxina);
        mediaPlayer.setLooping(true);

        alertDialog = new Dialog(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.booking_request_pop_up);

        accept_btn= (Button) alertDialog.findViewById(R.id.accept_btn);
        reject_btn= (Button) alertDialog.findViewById(R.id.reject_btn);

        mTVBid= (TextView) alertDialog.findViewById(R.id.tvBid);
        mTvDate= (TextView) alertDialog.findViewById(R.id.tvDate);
        mTvPickup= (TextView) alertDialog.findViewById(R.id.tvPick_up_address);
        mTvApptTime= (TextView) alertDialog.findViewById(R.id.tvApptTime);
        mTvApptDate= (TextView) alertDialog.findViewById(R.id.tvApptDate);

        String apptDate[]=appointmentDetailData.getApptDt().toString().split(" ");
        mTVBid.setText(getResources().getString(R.string.bookingidtext)+" "+appointmentDetailData.getBid());
        mTvDate.setText(" "+apptDate[0]);
        mTvPickup.setText(appointmentDetailData.getAddr1());
        mTvApptTime.setText(" "+apptDate[1]);

        startTimer(i,alertDialog);
        /*
         * For custom color only using layerdrawable to fill the star colors
         */
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
                if (connectionDetector.isConnectingToInternet())
                {
                    if(mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                    }
                    sendNotificationTOPassenger(6);
                }
                isRequested=false;
                alertDialog.dismiss();
            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }
                getRejectStatus(3);
                isRequested=false;
                alertDialog.dismiss();
            }
        });
        isRequested=true;
        mediaPlayer.start();
        alertDialog.show();
    }

///////////////////////////////////////////////////////////////////////////////////////////////////

    private void startTimer(int j, final Dialog alertDialog) {

        circularProgressBar = (CircularProgressBar)alertDialog.findViewById(R.id.circular_progress_bar);
        circularProgressBar.setProgress(100);

        textView= (TextView) alertDialog.findViewById(R.id.timer);
        //Toast.makeText(MainActivity.this, "Timer Started.!", Toast.LENGTH_SHORT).show();

        final int finalTime = j;
        mCounter=new CountDownTimer(finalTime *1000, 1000) {

            public void onTick(long millisUntilFinished) {

//                textView.setText("seconds remaining: " + millisUntilFinished / 1000);
                float i=((millisUntilFinished / 1000));
                float res=(i/ finalTime)*100;
                Log.d("timer_hai", " " + i + "\t" + res);
                circularProgressBar.setProgress(res);
                mm=(int)(i/60);
                ss=(int)(i%60);

//                textView.setText("seconds remaining: " +((int)(i/60))+":"+((int)(i%60)));

                if(mm<10 && ss<10 ){
                    resp="0"+mm+":0"+ss;
                }else if(mm>10 && ss<10){
                    resp=mm+":0"+ss;
                }else if(mm<10 && ss>10){
                    resp="0"+mm+":"+ss;
                }else if(mm>10 && ss>10){
                    resp=""+mm+ss;
                }
                textView.setText("" + resp );

            }

            public void onFinish() {
                try {
                    circularProgressBar.setProgress(0);
                    textView.setText("00:00");
                    alertDialog.dismiss();
                    isRequested=false;
                    if(mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }.start();

    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    public void setCarMarker(final Location location)
    {
        mCurrentLoc=location;

		if(mPreviousLoc==null)
		{
			mPreviousLoc=location;
		}

		final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
		if(marker!=null)
		{

            /*carMarker.setPosition(new LatLng(lat, lng));
            carMarker.setAnchor(0.5f, 0.5f);
            carMarker.setRotation(bearing);
            carMarker.setFlat(true);*/

			final Handler handler = new Handler();
			final long start = SystemClock.uptimeMillis();
			Projection proj = googleMap.getProjection();
			Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
			final LatLng startLatLng = proj.fromScreenLocation(startPoint);
			final long duration = 500;

			final Interpolator interpolator = new LinearInterpolator();

			handler.post(new Runnable() {
				@Override
				public void run() {
					long elapsed = SystemClock.uptimeMillis() - start;
					float t = interpolator.getInterpolation((float) elapsed
							/ duration);
					double lng = t * mCurrentLoc.getLongitude() + (1 - t)
							* startLatLng.longitude;
					double lat = t * mCurrentLoc.getLatitude() + (1 - t)
							* startLatLng.latitude;
                    marker.getmMarker().setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
                    marker.getmMarker().setAnchor(0.5f, 0.5f);
                    marker.getmMarker().setRotation(bearing);
                    marker.getmMarker().setFlat(true);


                    if (t < 1.0) {
						// Post again 16ms later.
						handler.postDelayed(this, 16);
					} else
                    {
						/*if (hideMarker) {
							marker.setVisible(false);
						} else {
							marker.setVisible(true);
						}*/
					}
				}
			});
		}

			mPreviousLoc=mCurrentLoc;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////

    public void setCarMarker(LatLng latLng)
    {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        googleMap.getUiSettings().setZoomControlsEnabled(false);

        if(marker!=null)
            marker.getmMarker().setPosition(latLng);
    }
}
